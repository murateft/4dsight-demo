package com.fourdsight;

import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.thymeleaf.ThymeleafAutoConfiguration;

@SpringBootApplication(exclude = {ThymeleafAutoConfiguration.class} )
@EnableRabbit
public class ScreenshotServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScreenshotServiceApplication.class, args);
    }
}
