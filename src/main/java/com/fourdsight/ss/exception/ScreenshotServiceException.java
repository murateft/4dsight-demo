package com.fourdsight.ss.exception;

import com.fourdsight.ss.model.OperationStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class ScreenshotServiceException extends Exception {

    public ScreenshotServiceException(String message) {
        super(message);
    }

    public ScreenshotServiceException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScreenshotServiceException(Throwable cause) {
        super(cause);
    }
}
