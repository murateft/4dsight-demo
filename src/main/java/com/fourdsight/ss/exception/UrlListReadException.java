package com.fourdsight.ss.exception;

public class UrlListReadException extends Exception {

    public UrlListReadException(String message) {
        super(message);
    }

    public UrlListReadException(String message, Throwable cause) {
        super(message, cause);
    }
}
