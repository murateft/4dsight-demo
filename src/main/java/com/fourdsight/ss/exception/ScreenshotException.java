package com.fourdsight.ss.exception;

public class ScreenshotException extends Exception {

    public ScreenshotException(String message) {
        super(message);
    }

    public ScreenshotException(String message, Throwable cause) {
        super(message, cause);
    }

    public ScreenshotException(Throwable cause) {
        super(cause);
    }
}
