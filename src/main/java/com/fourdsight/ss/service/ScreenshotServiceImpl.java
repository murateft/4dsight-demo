package com.fourdsight.ss.service;

import com.fourdsight.ss.exception.ScreenshotException;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class ScreenshotServiceImpl implements ScreenshotService {

    @Value("${chromedriver.url}")
    private String chromeDriverUrl;

    @Override
    public List<byte[]> takeScreenshot(Set<String> websiteUrls) throws ScreenshotException {

        try {
            log.debug("Chrome is starting in headless and full screen mode..");

            // use headless chrome and try to full screen
            ChromeOptions options = new ChromeOptions();
            options.addArguments(Arrays.asList("--window-size=1366x768", "--headless", "--disable-gpu", "--no-sandbox"));

            RemoteWebDriver chromeDriver = new RemoteWebDriver(new URL(chromeDriverUrl), options);
            List<byte[]> ssListInBytes = new ArrayList<>();

            // set timeout for chrome driver to get url
            // we can play with this value in some situations
            chromeDriver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

            // try to take screenshot for each website url
            for (String url : websiteUrls) {

                log.debug("Taking screenshot for url ({})..", url);

                chromeDriver.get(url);
                ssListInBytes.add(chromeDriver.getScreenshotAs(OutputType.BYTES));

                log.debug("Done as successfully for url ({}).", url);
            }

            chromeDriver.quit();
            log.debug("Chrome driver is quit successfully.");

            return ssListInBytes;

        } catch (Exception e) {
            throw new ScreenshotException(e);
        }
    }
}
