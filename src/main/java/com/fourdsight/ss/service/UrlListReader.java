package com.fourdsight.ss.service;

import com.fourdsight.ss.exception.UrlListReadException;

import java.io.InputStream;
import java.util.Set;

public interface UrlListReader {

    Set<String> readFromInputStream(InputStream inputStream) throws UrlListReadException;
}
