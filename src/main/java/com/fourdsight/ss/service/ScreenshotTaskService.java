package com.fourdsight.ss.service;

import com.fourdsight.ss.entity.Task;
import com.fourdsight.ss.exception.ScreenshotServiceException;
import com.fourdsight.ss.exception.UrlListReadException;
import com.fourdsight.ss.model.FileCharacteristic;
import com.fourdsight.ss.model.UrlSet;
import com.fourdsight.ss.config.RabbitMQConfig;
import com.fourdsight.ss.repository.TaskRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Exchange;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ScreenshotTaskService {

    @NonNull
    private final UrlListReader urlListReader;

    @NonNull
    private final ScreenshotService screenshotService;

    @NonNull
    private final RabbitTemplate rabbitTemplate;

    @NonNull
    private final Exchange exchange;

    @NonNull
    private final TaskDataService taskDataService;

    @NonNull
    private final ScreenshotPersistenceService screenshotPersistenceService;

    public Task start(InputStream urlListFileInputStream) throws ScreenshotServiceException {

        String taskId = null;

        try {
            log.debug("Task is starting..");

            // get url list from input stream
            Set<String> urls = urlListReader.readFromInputStream(urlListFileInputStream);

            log.debug("Urls are loaded successfully.. {}", urls);

            // create new task and save to db
            Task task = new Task();
            task.setCreatedDate(LocalDateTime.now());
            task.setStatus(Task.Status.PROCESSING);
            task = taskDataService.saveTask(task);
            taskId = task.getId().toString();

            log.debug("Task id assigned.. Task Id: {}", taskId);

            // wrap unique id and its urls
            UrlSet urlSet = new UrlSet(taskId, urls);

            // send urls to queue to be consumed
            log.debug("Urls are sending to queue to to be processed..");
            rabbitTemplate.convertAndSend(exchange.getName(), "screenshot.created", urlSet);

            return task;

        } catch (Exception e) {

            // if we get an exception, marks task as ERROR
            if (taskId != null) {
                taskDataService.setStatus(UUID.fromString(taskId), Task.Status.ERROR);
            }

            if (e instanceof UrlListReadException) {
                throw new ScreenshotServiceException(e.getMessage(), e);
            }

            throw new ScreenshotServiceException(e);
        }
    }

    @RabbitListener(queues = RabbitMQConfig.SCREENSHOT_SERVICE_QUEUE)
    public void consumeFromQueue(UrlSet urlSet) throws ScreenshotServiceException {

        if (urlSet.getId() != null && urlSet.getUrls() != null && !urlSet.getUrls().isEmpty()) {

            try {
                log.debug("Url set {} came to consumer. Processing..", urlSet);

                List<FileCharacteristic> screenshotZipEntries = new ArrayList<>();

                // taske screenshots and get their bytes
                List<byte[]> ssBytes = screenshotService.takeScreenshot(urlSet.getUrls());

                // create zip entries of each ss file
                int i=0;
                for (String url : urlSet.getUrls()) {
                    screenshotZipEntries.add(new FileCharacteristic(url.length() > 100 ? url.substring(0, 100) : url,
                            "jpg", ssBytes.get(i)));
                    ++i;
                }

                screenshotPersistenceService.persist(urlSet.getId(), screenshotZipEntries);

                // if everything is ok, marks task as SUCCESS
                taskDataService.setStatus(UUID.fromString(urlSet.getId()), Task.Status.SUCCESS);

            } catch (Exception e) {

                // if we get an exception, marks task as ERROR
                taskDataService.setStatus(UUID.fromString(urlSet.getId()), Task.Status.ERROR);

                throw new ScreenshotServiceException(e);
            }

        } else {
            throw new ScreenshotServiceException("Id of url set could not be found while is consumed!");
        }
    }
}
