package com.fourdsight.ss.service;

import com.fourdsight.ss.entity.Task;
import com.fourdsight.ss.exception.ScreenshotServiceException;
import com.fourdsight.ss.repository.TaskRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class TaskDataService {

    @NonNull
    private final TaskRepository taskRepository;

    public Task setStatus(UUID taskId, Task.Status newStatus) throws ScreenshotServiceException {
        Optional<Task> task = taskRepository.findById(taskId);

        if (!task.isPresent())
            throw new ScreenshotServiceException("Task could not be found!");

        task.get().setStatus(newStatus);
        return taskRepository.save(task.get());
    }

    public Task saveTask(Task task) {
        return taskRepository.save(task);
    }

    public Optional<Task> getTaskById(UUID taskId) {
        return taskRepository.findById(taskId);
    }
}
