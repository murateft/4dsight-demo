package com.fourdsight.ss.service;

import com.fourdsight.ss.exception.UrlListReadException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UrlListMultipartReader implements UrlListReader {

    @Override
    public Set<String> readFromInputStream(InputStream inputStream) throws UrlListReadException {

        List<String> urls = new BufferedReader(new InputStreamReader(inputStream,
                StandardCharsets.UTF_8)).lines().collect(Collectors.toList());

        // If urls are in valid format, checks for each url
        for (String url : urls) {
            try {
                new URL(url);

            } catch (MalformedURLException e) {
                throw new UrlListReadException("Some of urls have invalid url format!");
            }
        }

        // duplicate urls are removed for efficiency if exists
        return new HashSet<>(urls);
    }
}
