package com.fourdsight.ss.service;

import com.fourdsight.ss.exception.ScreenshotException;

import java.util.List;
import java.util.Set;

public interface ScreenshotService {

    List<byte[]> takeScreenshot(Set<String> websiteUrls) throws ScreenshotException;
}
