package com.fourdsight.ss.service;

import com.fourdsight.ss.entity.Task;
import com.fourdsight.ss.exception.ScreenshotServiceException;
import com.fourdsight.ss.model.FileCharacteristic;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class ScreenshotPersistenceService {

    public static final String SS_ARCHIVE_STORAGE_PATH = "ss-archive";

    public void persist(String archiveId, List<FileCharacteristic> screenshotZipEntries)
            throws ScreenshotServiceException {

        try {
            // ss files convert to zip bytes
            byte[] ssZipFileBytes = toZipBytes(screenshotZipEntries);

            // write zip file to file system
            Files.write(Paths.get(SS_ARCHIVE_STORAGE_PATH + "/" +
                    generateArchiveFilename(archiveId) + ".zip"), ssZipFileBytes);

        } catch (IOException e) {
            throw new ScreenshotServiceException("An error is occurred while archive is persisted..", e);
        }
    }

    public byte[] toZipBytes(List<FileCharacteristic> screenshotZipEntries) throws IOException {

        try (ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
             ZipOutputStream zipOut = new ZipOutputStream(byteArrayOut)) {

            ZipEntry zipEntry;

            for (FileCharacteristic ssEntry : screenshotZipEntries) {

                zipEntry = new ZipEntry(ssEntry.getFilenameIncludesType());
                zipOut.putNextEntry(zipEntry);
                zipOut.write(ssEntry.getFileBytes());
                zipOut.closeEntry();
            }

            zipOut.flush();
            zipOut.finish();
            zipOut.close();

            return byteArrayOut.toByteArray();
        }
    }

    public FileCharacteristic getScreenshotArchiveInBytes(String taskId) throws ScreenshotServiceException {

        String archiveFilename = generateArchiveFilename(taskId);

        try {
            File archiveFile = ResourceUtils.getFile(SS_ARCHIVE_STORAGE_PATH + "/" + archiveFilename
                    + ".zip");
            return new FileCharacteristic(archiveFilename, "zip", Files.readAllBytes(archiveFile.toPath()));

        } catch (IOException e) {

            if (e instanceof FileNotFoundException)
                throw new ScreenshotServiceException("There is no created archive file!", e);

            throw new ScreenshotServiceException("An error is occurred while reading archive file!", e);
        }
    }

    private String generateArchiveFilename(String archiveId) {
        return "ss-" + archiveId;
    }
}
