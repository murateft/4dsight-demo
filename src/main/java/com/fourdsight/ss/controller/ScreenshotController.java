package com.fourdsight.ss.controller;

import com.fourdsight.ss.entity.Task;
import com.fourdsight.ss.exception.ScreenshotServiceException;
import com.fourdsight.ss.model.FileCharacteristic;
import com.fourdsight.ss.model.OperationStatus;
import com.fourdsight.ss.service.ScreenshotPersistenceService;
import com.fourdsight.ss.service.ScreenshotTaskService;
import com.fourdsight.ss.service.TaskDataService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.entity.ContentType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Optional;
import java.util.UUID;

@RestController
@RequestMapping("/screenshot-service")
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
@Slf4j
public class ScreenshotController {

    @NonNull
    private final ScreenshotTaskService screenshotTaskService;

    @NonNull
    private final ScreenshotPersistenceService screenshotPersistenceService;

    @NonNull
    private final TaskDataService taskDataService;

    @PostMapping("/upload")
    public ResponseEntity<?> uploadFileOfUrls(@RequestParam("file") MultipartFile file) {

        try {
            Task task = screenshotTaskService.start(file.getInputStream());
            return new ResponseEntity<>(OperationStatus.builder()
                        .taskId(task.getId().toString())
                        .status(task.getStatus())
                        .build(),
                    HttpStatus.OK);

        } catch (Exception e) {
            log.error("File uploading is failed!", e);
            return new ResponseEntity<>(OperationStatus.builder()
                        .status(Task.Status.ERROR)
                        .message(e.getMessage())
                        .build(),
                    HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/download/{taskId}")
    public ResponseEntity<?> downloadScreenshotFiles(@PathVariable("taskId") String taskId) {

        // if we had this task?
        Optional<Task> task = taskDataService.getTaskById(UUID.fromString(taskId));
        if (!task.isPresent())
            return new ResponseEntity<>(OperationStatus.builder()
                        .status(Task.Status.ERROR)
                        .message("No task is found!")
                        .build(),
                    HttpStatus.BAD_REQUEST);

        // get archive file that contains screenshots
        FileCharacteristic fileCharacteristic;

        // If task is completed successfully?
        if (task.get().getStatus() == Task.Status.SUCCESS) {

            try {
                fileCharacteristic = screenshotPersistenceService.getScreenshotArchiveInBytes(taskId);

                // make downloadable zip file
                HttpHeaders responseHeaders = new HttpHeaders();
                responseHeaders.setContentDisposition(ContentDisposition.builder("zip")
                        .filename(fileCharacteristic.getFilenameIncludesType())
                        .build());
                responseHeaders.add("Content-Type","application/zip");

                return new ResponseEntity<>(fileCharacteristic.getFileBytes(), responseHeaders, HttpStatus.OK);

            } catch (ScreenshotServiceException e) {
                return new ResponseEntity<>(OperationStatus.builder()
                        .taskId(taskId)
                        .status(Task.Status.ERROR)
                        .build(),
                        HttpStatus.OK);
            }
        } else {
            return new ResponseEntity<>(OperationStatus.builder()
                    .taskId(taskId)
                    .status(task.get().getStatus())
                    .build(),
                    HttpStatus.OK);
        }
    }
}