package com.fourdsight.ss.model;

import com.fourdsight.ss.entity.Task;
import lombok.*;

@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class OperationStatus {

    private String taskId;

    private Task.Status status;

    private String message;
}
