package com.fourdsight.ss.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Set;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UrlSet implements Serializable {
    private static final long serialVersionUID = 1L;

    private String id;

    private Set<String> urls;
}
