package com.fourdsight.ss.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.util.StringUtils;

@Data
public class FileCharacteristic {

    private String filename;

    private String fileType;

    private byte[] fileBytes;

    public FileCharacteristic(String filename, String fileType, byte[] fileBytes) {
        setFilename(filename);
        this.fileType = fileType;
        this.fileBytes = fileBytes;
    }

    public void setFilename(String filename) {
        this.filename = filename != null ? filename.replaceAll("https://|www.|http://", "") : null;
    }

    public String getFilenameIncludesType() {
        return filename + "." + fileType;
    }
}
